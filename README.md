# README #


### What is this repository for? ###
This project is a Java implementation of Tomasulo Algorithm, It was the microprocessors's course project at the university. I was a member of team of 5 who worked on this project. 

Tomasulo�s algorithm is a computer architecture hardware algorithm for dynamic scheduling of instructions that allows out-of-order execution and enables more efficient use of multiple execution units. It was developed by Robert Tomasulo at IBM in 1967 and was first implemented in the IBM System/360 Model 91�s floating point unit.

### How do I get set up? ###
Import the zipped file into the java framework you are working with, run Main.java class and follow the instructions.
